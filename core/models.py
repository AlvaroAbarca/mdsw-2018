from django.urls import reverse
from django_extensions.db.fields import AutoSlugField
from django.db.models import *
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db import models as models
from django_extensions.db import fields as extension_fields
from datetime import date
from core.defines import *



class Central(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=50)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_central_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_central_update', args=(self.slug,))


    def get_delete_url(self):
        return reverse('core_asistencia_delete', args=(self.slug,))

class Emergencia(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=20, choices = type_emergency)
    level = models.CharField(max_length=20,choices = level_emergency)
    address = models.CharField(max_length=30)
    description = models.TextField(max_length=255, blank = True, null = True)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('-created',)
    def getName(self):
        return self.name
    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_emergencia_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_emergencia_update', args=(self.slug,))


    def get_delete_url(self):
        return reverse('core_asistencia_delete', args=(self.slug,))


class Cuadrilla(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    status = models.CharField(max_length=30, choices = estados)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    # Relationship Fields
    central = models.ForeignKey(Central, on_delete=models.CASCADE, editable = True, default = 'CENTRAL')


    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_cuadrilla_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_cuadrilla_update', args=(self.slug,))

    
    def get_delete_url(self):
        return reverse('core_asistencia_delete', args=(self.slug,))

class Tecnico(models.Model):

    # Fields
    name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    rut = models.CharField(max_length=10,primary_key=True, help_text='12345678-9')
    birth_date = models.DateField()
    speciality = models.CharField(max_length=30, choices= type_tecnico)
    status = models.CharField(max_length=10, choices = estados, blank = True, null = True)
    slug = extension_fields.AutoSlugField(populate_from='rut', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    #Relationship Fields
    cuadrilla = models.ForeignKey(Cuadrilla,on_delete=models.CASCADE, blank = True, null = True)


    class Meta:
        ordering = ('-created',)
    def cname(self):
        if (self.cuadrilla != None):
            return self.cuadrilla.name
        else: return None
    def full_name(self):
        return '{} {}' . format(self.name, self.last_name)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_tecnico_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('core_tecnico_update', args=(self.slug,))

    def get_delete_url(self):
        return reverse('core_tecnico_delete', args=(self.slug,))



class Administrador(models.Model):

    # Fields
    name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    rut = models.CharField(max_length=10)
    birth_date = models.DateField()
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    
    class Meta:
        ordering = ('-created',)

    def full_name(self):
        return '{} {}' . format(self.name, self.last_name)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_administrador_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('core_administrador_update', args=(self.slug,))

class Asistencia(models.Model):

    # Fields
    status = models.CharField(max_length=15, choices = completado, blank = True, null = True)
    slug = extension_fields.AutoSlugField(populate_from='cuadrilla', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    # Relationship Fields
    cuadrilla = models.ForeignKey(Cuadrilla,on_delete=models.CASCADE)
    emergencia = models.ForeignKey(Emergencia,on_delete=models.CASCADE)

    class Meta:
        ordering = ('-created',)

    def getcname(self):
        if (self.cuadrilla != None):
            return self.cuadrilla.name
        else: return None

    def getename(self):
        if(self.emergencia != None):
            return self.emergencia.name
        else: return None
    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('core_asistencia_detail', args=(self.slug,))

    
    def get_update_url(self):
        return reverse('core_asistencia_update', args=(self.slug,))

    def get_delete_url(self):
        return reverse('core_asistencia_delete', args=(self.slug,))
