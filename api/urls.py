from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register('users',views.UserViewSet)
router.register('groups',views.GroupViewSet)

urlpatterns = [
	path('',include(router.urls)),
] 