from django.shortcuts import render
from core.models import *
from rest_framework import generics, viewsets
from django.contrib.auth.models import User
from api.serializers import *
from django.shortcuts import get_object_or_404

class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all().order_by('-date_joined')
	serializer_class = UserSerializer
"""
	def obj(self):
		queryset = self.get_queryset()
		obj = get_object_or_404(
			queryset,
			pk =self.kwargs['pk'],
			)
		return obj """
class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer