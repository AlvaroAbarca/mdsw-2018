from django.contrib import admin
from django import forms
from .models import Tecnico, Cuadrilla, Administrador, Central, Emergencia, Asistencia

class TecnicoAdminForm(forms.ModelForm):

    class Meta:
        model = Tecnico
        fields = '__all__'


class TecnicoAdmin(admin.ModelAdmin):
    form = TecnicoAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'speciality']
    readonly_fields = ['name', 'slug', 'created', 'last_updated', 'speciality']

admin.site.register(Tecnico, TecnicoAdmin)


class CuadrillaAdminForm(forms.ModelForm):

    class Meta:
        model = Cuadrilla
        fields = '__all__'


class CuadrillaAdmin(admin.ModelAdmin):
    form = CuadrillaAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'status']
    readonly_fields = ['name', 'slug', 'created', 'last_updated', 'status']

admin.site.register(Cuadrilla, CuadrillaAdmin)


class AdministradorAdminForm(forms.ModelForm):

    class Meta:
        model = Administrador
        fields = '__all__'


class AdministradorAdmin(admin.ModelAdmin):
    form = AdministradorAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'last_name', 'rut', 'birth_date']
    readonly_fields = ['name', 'slug', 'created', 'last_updated', 'last_name', 'rut', 'birth_date']

admin.site.register(Administrador, AdministradorAdmin)


class CentralAdminForm(forms.ModelForm):

    class Meta:
        model = Central
        fields = '__all__'


class CentralAdmin(admin.ModelAdmin):
    form = CentralAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'address']
    readonly_fields = ['name', 'slug', 'created', 'last_updated', 'address']

admin.site.register(Central, CentralAdmin)


class EmergenciaAdminForm(forms.ModelForm):

    class Meta:
        model = Emergencia
        fields = '__all__'


class EmergenciaAdmin(admin.ModelAdmin):
    form = EmergenciaAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'type', 'level', 'address']
    readonly_fields = ['name', 'slug', 'created', 'last_updated', 'type', 'level', 'address']

admin.site.register(Emergencia, EmergenciaAdmin)


class AsistenciaAdminForm(forms.ModelForm):

    class Meta:
        model = Asistencia
        fields = '__all__'


class AsistenciaAdmin(admin.ModelAdmin):
    form = AsistenciaAdminForm
    list_display = ['slug', 'created', 'last_updated']
    readonly_fields = ['slug', 'created', 'last_updated']

admin.site.register(Asistencia, AsistenciaAdmin)


