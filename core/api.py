from . import models
from . import serializers
from rest_framework import viewsets, permissions


class TecnicoViewSet(viewsets.ModelViewSet):
    """ViewSet for the Tecnico class"""

    queryset = models.Tecnico.objects.all().order_by('cuadrilla')
    serializer_class = serializers.TecnicoSerializer
    permission_classes = [permissions.IsAuthenticated]


class CuadrillaViewSet(viewsets.ModelViewSet):
    """ViewSet for the Cuadrilla class"""

    queryset = models.Cuadrilla.objects.all()
    serializer_class = serializers.CuadrillaSerializer
    permission_classes = [permissions.IsAuthenticated]


class AdministradorViewSet(viewsets.ModelViewSet):
    """ViewSet for the Administrador class"""

    queryset = models.Administrador.objects.all()
    serializer_class = serializers.AdministradorSerializer
    permission_classes = [permissions.IsAuthenticated]


class CentralViewSet(viewsets.ModelViewSet):
    """ViewSet for the Central class"""

    queryset = models.Central.objects.all()
    serializer_class = serializers.CentralSerializer
    permission_classes = [permissions.IsAuthenticated]


class EmergenciaViewSet(viewsets.ModelViewSet):
    """ViewSet for the Emergencia class"""

    queryset = models.Emergencia.objects.all()
    serializer_class = serializers.EmergenciaSerializer
    permission_classes = [permissions.IsAuthenticated]


class AsistenciaViewSet(viewsets.ModelViewSet):
    """ViewSet for the Asistencia class"""

    queryset = models.Asistencia.objects.all()
    serializer_class = serializers.AsistenciaSerializer
    permission_classes = [permissions.IsAuthenticated]


