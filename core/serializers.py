from . import models

from rest_framework import serializers


class TecnicoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Tecnico
        fields = (
            'full_name', 
            'rut', 
            'birth_date', 
            'speciality', 
            'cname',
            'status', 
        )


class CuadrillaSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Cuadrilla
        fields = (
            'slug', 
            'name', 
            'created', 
            'last_updated', 
            'status', 
        )


class AdministradorSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Administrador
        fields = (
            'slug', 
            'name', 
            'created', 
            'last_updated', 
            'last_name', 
            'rut', 
            'birth_date', 
        )


class CentralSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Central
        fields = ( 
            'name', 
            'address', 
            'created', 
            'last_updated', 
        )


class EmergenciaSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Emergencia
        fields = (
            'slug', 
            'name', 
            'created', 
            'last_updated', 
            'type', 
            'level', 
            'address', 
        )


class AsistenciaSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Asistencia
        fields = (
            'slug', 
            'created', 
            'last_updated',
            'getcname',
            'getename', 
        )


