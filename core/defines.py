type_tecnico = (
    ('Alto','Alto'),
    ('Mediano','Mediano'),
    ('Bajo','Bajo'),
    )
type_emergency = (
	('A','A'),
	('B','B'),
	('C','C'),
	('D','D'),
	('E','E'),
	)
level_emergency = (
	('1','1'),
	('2','2'),
	('3','3'),
	('4','4'),
	('5','5'),
	)
estados = (
	('Activo','Activo'),
	('Inactivo','Inactivo'),
	)
completado = (
	('Por asignar','Por asignar'),
	('En Proceso','En Proceso'),
	('Finalizado','Finalizado'),
	)