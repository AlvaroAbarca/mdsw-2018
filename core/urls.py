from django.urls import path, include
from rest_framework import routers

from . import api
from . import views

router = routers.DefaultRouter()
router.register(r'tecnico', api.TecnicoViewSet)
router.register(r'cuadrilla', api.CuadrillaViewSet)
router.register(r'administrador', api.AdministradorViewSet)
router.register(r'central', api.CentralViewSet)
router.register(r'emergencia', api.EmergenciaViewSet)
router.register(r'asistencia', api.AsistenciaViewSet)



urlpatterns = (
    # urls for Django Rest Framework API
    path('api/v1/', include(router.urls)),
)

urlpatterns += (
    # urls for Tecnico
    path('core/tecnico/', views.TecnicoListView.as_view(), name='core_tecnico_list'),
    path('core/tecnico/all/', views.TecnicoListViewAll.as_view(), name='core_tecnico_list_all'),
    path('core/tecnico/create/', views.TecnicoCreateView.as_view(), name='core_tecnico_create'),
    path('core/tecnico/detail/<slug:slug>/', views.TecnicoDetailView.as_view(), name='core_tecnico_detail'),
    path('core/tecnico/update/<slug:slug>/', views.TecnicoUpdateView.as_view(), name='core_tecnico_update'),
    path('core/tecnico/delete/<slug:slug>/', views.TecnicoDeleteView.as_view(), name='core_tecnico_delete'),
)

urlpatterns += (
    # urls for Cuadrilla
    path('core/cuadrilla/', views.CuadrillaListView.as_view(), name='core_cuadrilla_list'),
    path('core/cuadrilla/create/', views.CuadrillaCreateView.as_view(), name='core_cuadrilla_create'),
    path('core/cuadrilla/detail/<slug:slug>/', views.CuadrillaDetailView.as_view(), name='core_cuadrilla_detail'),
    path('core/cuadrilla/update/<slug:slug>/', views.CuadrillaUpdateView.as_view(), name='core_cuadrilla_update'),
)

urlpatterns += (
    # urls for Administrador
    path('core/administrador/', views.AdministradorListView.as_view(), name='core_administrador_list'),
    path('core/administrador/create/', views.AdministradorCreateView.as_view(), name='core_administrador_create'),
    path('core/administrador/detail/<slug:slug>/', views.AdministradorDetailView.as_view(), name='core_administrador_detail'),
    path('core/administrador/update/<slug:slug>/', views.AdministradorUpdateView.as_view(), name='core_administrador_update'),
)

urlpatterns += (
    # urls for Central
    path('core/central/', views.CentralListView.as_view(), name='core_central_list'),
    path('core/central/create/', views.CentralCreateView.as_view(), name='core_central_create'),
    path('core/central/detail/<slug:slug>/', views.CentralDetailView.as_view(), name='core_central_detail'),
    path('core/central/update/<slug:slug>/', views.CentralUpdateView.as_view(), name='core_central_update'),
)

urlpatterns += (
    # urls for Emergencia
    path('core/emergencia/', views.EmergenciaListView.as_view(), name='core_emergencia_list'),
    path('core/emergencia/create/', views.EmergenciaCreateView.as_view(), name='core_emergencia_create'),
    path('core/emergencia/detail/<slug:slug>/', views.EmergenciaDetailView.as_view(), name='core_emergencia_detail'),
    path('core/emergencia/update/<slug:slug>/', views.EmergenciaUpdateView.as_view(), name='core_emergencia_update'),
)

urlpatterns += (
    # urls for Asistencia
    path('core/asistencia/', views.AsistenciaListView.as_view(), name='core_asistencia_list'),
    path('core/asistencia/create/', views.AsistenciaCreateView.as_view(), name='core_asistencia_create'),
    path('core/asistencia/detail/<slug:slug>/', views.AsistenciaDetailView.as_view(), name='core_asistencia_detail'),
    path('core/asistencia/update/<slug:slug>/', views.AsistenciaUpdateView.as_view(), name='core_asistencia_update'),
    path('core/asistencia/delete/<slug:slug>/', views.AsistenciaDeleteView.as_view(), name='core_asistencia_delete'),
)

urlpatterns += (
    path ('core/',views.index, name = 'index'),
    )