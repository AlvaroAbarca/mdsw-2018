from django.views.generic import DetailView, ListView, UpdateView, CreateView
from django.views.generic.edit import DeleteView
from .models import Tecnico, Cuadrilla, Administrador, Central, Emergencia, Asistencia
from .forms import TecnicoForm, CuadrillaForm, AdministradorForm, CentralForm, EmergenciaForm, AsistenciaForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy

def index(request):
    template_name = 'index.html'
    data = {}
    return render(request,template_name,data)

class TecnicoListView(ListView):
    model = Tecnico
    template_name = 'core/tecnico_list.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'object_list'  # Default: object_list
    paginate_by = 10
    #queryset = Tecnico.objects.all()  # Default: Model.objects.all()
    queryset = Tecnico.objects.filter(status='Activo')  # Default: Model.objects.all()

class TecnicoListViewAll(ListView):
    model = Tecnico
    template_name = 'core/tecnico_list.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'object_list'  # Default: object_list
    paginate_by = 10
    queryset = Tecnico.objects.all()  # Default: Model.objects.all()
    #queryset = Tecnico.objects.filter(status='Activo')  # Default: Model.objects.all()

class TecnicoCreateView(CreateView):
    model = Tecnico
    form_class = TecnicoForm


class TecnicoDetailView(DetailView):
    model = Tecnico


class TecnicoUpdateView(UpdateView):
    model = Tecnico
    form_class = TecnicoForm
    form_valid_message = 'Tecnico Actualizado Correctamente.'
    form_invalid_message = 'Existen Algunos Errores.'

class TecnicoDeleteView(DeleteView):
    model = Tecnico
    success_url = reverse_lazy('core_tecnico_list')

class CuadrillaListView(ListView):
    model = Cuadrilla
    template_name = 'core/cuadrilla_list.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'object_list'  # Default: object_list
    paginate_by = 10
    queryset = Cuadrilla.objects.all()  # Default: Model.objects.all()


class CuadrillaCreateView(CreateView):
    model = Cuadrilla
    form_class = CuadrillaForm


class CuadrillaDetailView(DetailView):
    model = Cuadrilla


class CuadrillaUpdateView(UpdateView):
    model = Cuadrilla
    form_class = CuadrillaForm

class CuadrillaDeleteView(DeleteView):
    model = Cuadrilla
    success_url = ('core_cuadrilla_list')

class AdministradorListView(ListView):
    model = Administrador
    template_name = 'core/administrador_list.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'object_list'  # Default: object_list
    paginate_by = 10
    queryset = Administrador.objects.all()  # Default: Model.objects.all()


class AdministradorCreateView(CreateView):
    model = Administrador
    form_class = AdministradorForm


class AdministradorDetailView(DetailView):
    model = Administrador


class AdministradorUpdateView(UpdateView):
    model = Administrador
    form_class = AdministradorForm

class AdministradorDeleteView(DeleteView):
    model = Administrador
    success_url = ('core_administrador_list')


class CentralListView(ListView):
    model = Central


class CentralCreateView(CreateView):
    model = Central
    form_class = CentralForm


class CentralDetailView(DetailView):
    model = Central


class CentralUpdateView(UpdateView):
    model = Central
    form_class = CentralForm


class EmergenciaListView(ListView):
    model = Emergencia
    template_name = 'core/emergencia_list.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'object_list'  # Default: object_list
    paginate_by = 10
    queryset = Emergencia.objects.all()  # Default: Model.objects.all()

class EmergenciaCreateView(CreateView):
    model = Emergencia
    form_class = EmergenciaForm


class EmergenciaDetailView(DetailView):
    model = Emergencia


class EmergenciaUpdateView(UpdateView):
    model = Emergencia
    form_class = EmergenciaForm

class EmergenciaDeleteView(DeleteView):
    model = Emergencia
    success_url = reverse_lazy('core_emergencia_list')

class AsistenciaListView(ListView):
    model = Asistencia
    template_name = 'core/asistencia_list.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'object_list'  # Default: object_list
    paginate_by = 10
    queryset = Asistencia.objects.all()  # Default: Model.objects.all()


class AsistenciaCreateView(CreateView):
    model = Asistencia
    form_class = AsistenciaForm


class AsistenciaDetailView(DetailView):
    model = Asistencia


class AsistenciaUpdateView(UpdateView):
    model = Asistencia
    form_class = AsistenciaForm

class AsistenciaDeleteView(DeleteView):
    model = Asistencia
    success_url = reverse_lazy('core_asistencia_list')

def CuadrillaComposeAdd(request):
    data = {}

    if request.method == "POST":
        players = request.POST.get("tecnicos[]")
        emergencia = request.POST["emergencia"]
        central = request.POST["central"]
        cuadrilla = request.POST["cuadrilla"]

        emergencia = Emergencia.objects.get(pk=int(emergencia))
        central = Emergencia.objects.get(pk=int(central))
        cuadrilla = Emergencia.objects.get(pk=int(cuadrilla))
        cuadrillacompose = CuadrillaCompose.objects.create(emergencia = emergencia, central = central, cuadrilla = cuadrilla)
        cuadrillacompose.save()
        for tecnico in tecnicos:
            tecnico = Tecnico.objects.get(pk=int(tecnico))
            cuadrillacompose.tecnico.add(tecnico)
            cuadrillacompose.save()
        template_name = 'core/cuadrillacompose_form.html'
        return render(request, template_name, data)

    else:
        tecnicos = Tecnico.objects.all()
        emergencias = Emergencia.objects.all()
        centrales = Central.objects.all()
        cuadrillas = Cuadrilla.objects.all()
        data["tecnicos"] = tecnicos
        data["emergencias"] = emergencias
        data["centrales"] = centrales
        data["cuadrillas"] = cuadrillas
    template_name = 'core/cuadrillacompose_form.html'
    return render(request,template_name,data)

def TecnicoDetalle(request,slug):
    template_name = 'tecnico_detail.html'
    slug = int(slug)
    data = {}
    data['object'] = Tecnico.objects.get(slug = slug)
    if request.method == "POST":
        data['form'] = TecnicoForm(request.POST,instance=slug)
        if data['form'].is_valid():
            data ['form'].save()
    else:
            data['form'] = TecnicoForm(instance=slug)
    return render(request,template_name,data)