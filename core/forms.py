from django import forms
from .models import Tecnico, Cuadrilla, Administrador, Central, Emergencia, Asistencia


class TecnicoForm(forms.ModelForm):
    class Meta:
        model = Tecnico
        fields = ['name', 'last_name', 'rut', 'birth_date','speciality','cuadrilla','status']
        widgets = {
            'birth_date': forms.DateInput(attrs={'class':'datepicker'}),
        }

class CuadrillaForm(forms.ModelForm):
    class Meta:
        model = Cuadrilla
        fields = ['name', 'status','central']


class AdministradorForm(forms.ModelForm):
    class Meta:
        model = Administrador
        fields = ['name', 'last_name', 'rut', 'birth_date']
        widgets = {
            'birth_date': forms.DateInput(attrs={'class':'datepicker'}),
        }


class CentralForm(forms.ModelForm):
    class Meta:
        model = Central
        fields = ['name', 'address']


class EmergenciaForm(forms.ModelForm):
    class Meta:
        model = Emergencia
        fields = ['name', 'type', 'level', 'address','description']


class AsistenciaForm(forms.ModelForm):
    class Meta:
        model = Asistencia
        fields = ['cuadrilla', 'emergencia','status']