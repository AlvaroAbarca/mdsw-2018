# -*- coding: utf-8 -*-
from telegram.ext import Updater, CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler

import time, json


token='600136558:AAEM-iaEKHNZ60mggwnB91HxBSYNJIANz_Q'

def start(bot, update):
    update.message.reply_text('Bienvenido al bot de consulta '+update.message.from_user.first_name)
    #update.message.reply_text("son las "+ time.strftime("%H:%M") +" y es "+ time.strftime("%d/%m/%Y")+'\n')
    update.message.reply_text('\n si tienes dudas utilice el comando /help\n')
    
	
def Help(bot, update):
    update.message.reply_text('Las consultas disponibles son las siguientes: \n' )
    update.message.reply_text('/disponible: Lista de trabajadores disponibles \n' )
    update.message.reply_text('/asignado: Lista de trabajadores asignados a contingencias\n' )
	
def obtenerDatos_DIS(usuario):
    try:
        json_file = open('http://localhost:8080/api/v1/tecnico/?format=json')
        data = json.load(json_file)
        json_file.close()
        return data
    except:
        print("Falla")
        
def obtenerDatos_ASIG(usuario):
    try:
        json_file = open('localhost.json')
        data = json.load(json_file)
        json_file.close()
        return data
    except:
        print("Falla")

def Asignados(bot, update):
    contador = 0
    asignaturas = obtenerDatos_DIS(update.message.from_user.first_name)
    update.message.reply_text('Lista de Trabajadores:\n')
    for a in asignaturas:
        if a["status"] == 'Activo':
            contador+=1
    if contador > 0:
        for b in asignaturas:
            if b["status"] == 'Activo':
                update.message.reply_text("Nombre: "+b["full_name"]+'\n'+"Rut: "+b["rut"]+'\n'+"Status: "+b["status"]+'\n'+"Especialidad: "+b["speciality"])
    else:
        update.message.reply_text("No hay trabajadores activos")

def Disponibles(bot, update):
    contador = 0
    asignaturas = obtenerDatos_DIS(update.message.from_user.first_name)
    update.message.reply_text('Lista de Trabajadores:\n')
    for a in asignaturas:
        if a["status"] == 'Inactivo':
            contador+=1
            
    if contador > 0:
        for b in asignaturas:
            if b["status"] == 'Inactivo':
                update.message.reply_text("Nombre: "+b["full_name"]+'\n'+"Rut: "+b["rut"]+'\n'+"Status: "+b["status"]+'\n'+"Especialidad: "+b["speciality"])
    else:
        update.message.reply_text("No hay trabajadores disponibles")

def button(bot, update):
    print("boton apretado")
    query = update.callback_query
    asignatura = query.data
    print (query.data)
    usuario = query.message.chat.username
    datos = obtenerDatos(usuario)

    msg = "Trabajador Seleccionado: "+ asignatura +"\n"
    for i in datos[asignatura]:
        msg += "\t"+ i +": "+ datos[asignatura][i] +"\n"
    bot.edit_message_text(text=msg, chat_id=query.message.chat_id, message_id=query.message.message_id)


updater = Updater(token)
updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(CommandHandler('help', Help))
updater.dispatcher.add_handler(CommandHandler('disponible', Disponibles))
updater.dispatcher.add_handler(CommandHandler('asignado', Asignados))
updater.dispatcher.add_handler(CallbackQueryHandler(button))

updater.start_polling()
updater.idle()
